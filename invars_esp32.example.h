const char* VERSION = "2016060602";
const bool DEBUG = 1;

const unsigned int SLEEPTIME = 1;

const char* WIFISSID = "myssid";
const char* WIFIPASS = "mypass";

const char* DATASERVER = "server";
const char* DATAURL = "/sensors";
const char* DATAUSER = "serveruser";
const char* DATAPASS = "serverpass";
