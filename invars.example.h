#define VERSION 2016060602
#define DEBUG 1

#define SLEEPTIME 15

#define WIFISSID myssid
#define WIFIPASS mypass

#define DATASERVER server
#define DATAURL /sensors
#define DATAUSER serveruser
#define DATAPASS serverpass
#define DEBUG 1

const String VERSION = "2016060602";

const String WIFI_SSID = "myssid";
const String WIFI_USER = "myuser";
const String WIFI_PASS = "mypass";

const unsigned long WIFI_TIMEOUT = 5000; // in milliseconds
// ESP8266 do not use this parameter.

const String DATA_HOST = "server";
const uint16_t DATA_PORT = 443;
const String DATA_URL = "/sensors/data";
const String DATA_USER = "serveruser";
const String DATA_PASS = "serverpass";

const unsigned long SLEEP_TIME = 60; // in seconds

// Certificate Authority for Wi-Fi.
const char* WIFI_CA = \
"-----BEGIN CERTIFICATE-----\n" \
"…\n" \
"-----END CERTIFICATE-----\n";

const char* DATA_CA = WIFI_CA; // They are the same.

// With ESP8266, l'authentification est basée sur l'empreinte.
const String DATA_FINGERPRINT = "XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX ;


// vim: ft=arduino
// vim: et sw=4 sts=4 sw=4
