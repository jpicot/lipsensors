# Hardware

Lipsensors currently run on two different boards:

1. Wemos _ESP8266_

   Associated temperature sensors:

   * AdaFruit _DHT11_ (Blue). [Product Page](https://www.adafruit.com/product/386)

   * AdaFruit _DHT22_ (White). [Product Page](https://www.adafruit.com/product/385)

2. Olimex _ESP32-S2-DevKit-Lipo_ rev. B1. [Product Page](https://www.olimex.com/Products/IoT/ESP32-S2/ESP32-S2-DevKit-Lipo-USB/open-source-hardware), [Github](https://github.com/OLIMEX/ESP32-S2-DevKit-LiPo-USB)

   Associated temperature sensors:

   * DFRobot _Sen0137_ [Product Page](https://www.dfrobot.com/product-1102.html)

      Runs with library **DHT sensor library for ESPx** by beegee_tokyo [Github](https://github.com/beegee-tokyo/DHTesp)

# Installation procedure

We don't use the _Arduino IDE_ nor _Platform IO_; instead we resort on
_makeEspArduino_ to build and flash boards with Makefile.

The following procedure consists mainly in cloning some repos and setting
environment variables.

1. Get **makeEspArduino** from Github to the directory of your choice.

    ```bash
    git clone https://github.com/plerup/makeEspArduino
    ```

2. Set the environment variable `MAKE_ESP` to the path of the
   `makeEspArduino.mk` script of the repository you have just cloned. This
   variable can be set in the Makefile.

Invoking make from this point will load scripts in `makeEspArduino.mk` here is
a subset of commands you can run:

* `make` — build the project application
* `make upload` — flash the ESP connected to your computer. The script should
  be able to find the appropriate device. If not, set `UPLOAD_PORT`
* `make help` — show help on all command and variables


## Procedure for _Wemos ESP8266_

3. Get an **ESP build environment** at, e.g. https://github.com/esp8266/Arduino
   and set Makefile's `ESP_ROOT` variable to this directory's path.

4. Run

      ```bash
      cd $ESP_ROOT/tools/
      python3 get.py
      ```

   to get the binaries.

   More detailed instructions:
   https://arduino-esp8266.readthedocs.io/en/latest/installing.html#using-git-version

5. Get the sensors' library at https://github.com/adafruit/DHT-sensor-library
   and set Makefile's `CUSTOM_LIBS` to the `DHT.h` file of the repository.

   **Note:** Use the version `1.2.3` of the library.

6. Create the `invars.h` file from `invars.example.h`.

## Procedure for _Olimex ESP32-S2_

Source: https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html#linux

3. Choose a place where to install ESP libraries.

   ```bash
   ESP_ROOT=…
   ```

4. Get an **ESP build environment** (~2 GiB !)

   ```bash
   git clone https://github.com/espressif/arduino-esp32 $ESP_ROOT
   ```

5. Get the **binaries** (+ 3.8 GiB ‼)

   ```bash
   cd $ESP_ROOT/tools/
   python3 get.py
   ```

6. Get the **DHTesp** thermal and humidity sensor library 

   ```bash
   cd $ESP_ROOT/libraries/
   git clone https://github.com/beegee-tokyo/DHTesp
   ```

   Do not create a _git subtree._


# File list

- firmware.cpp and firmware32.cpp: Source code of firmware deployed on sensors.
- invars.h: Variables used in firmware.cpp, see invars.example.h.
- lipsensors_server.py: Python HTTP server used for receiving sensors' messages
  and transfering them to collectd daemon. Also used for remote update of
  sensors' firmware.
- Makefile
- firmware/: Directory containing firmware for remote sensor update.

# Environment variables

Environment variables can be set in the Makefile.

- `ESP_ROOT` — Path to ESP build environment.
- `MAKE_ESP` — Path to makeEspArduino.mk, the Makefile script to compile and
             flash code.
- `SKETCH` — Filename to the source code of the firmware.
- `CHIP` — Chip model, either esp8266 or esp32.
- `BOARD` — Board, either  nodemcu or esp32s2
- `UPLOAD_SPEED` — 115200
- `UPLOAD_PORT` — Usually /dev/ttyUSB0 or /dev/ttyUSB1

# Usage

Remember: the variable `MAKE_ESP` in the Makefile should be set to the path of
`makeEspArduino.mk` to work properly.

- Set the target hardware:

  - `CHIP=esp8266`, or empty, for the Wemos _ESP8266_
  - `CHIP=esp32` for the Olimex _ESP32-S2-DevKit-Lipo_

  In the former case, you must also set the temperature sensor:

  - `DHTTYPE=DHT22` for the large, white sensor
  - `DHTTYPE=DHT12` for the small, blue sensor

  These variables can be passed throur the make command line.

- Build `firmware/firmware.bin`

        make clean && make

- Upload `firmware/firmware.bin` on sensor connected to `/dev/ttyUSB0`

        sudo make upload

  You will need the python package `esptool`.

- Copy firmware.bin to firmware/firmware-<version>.bin, using
  <version> declared in "invars.h" file. Used for OTA sensor update.

        make publish


# Python HTTP server

Module dependencies:
- flask 
- pyonf


# OTA update
TODO
